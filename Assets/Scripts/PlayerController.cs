﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

public float speed;
public Text countText;

private Rigidbody rb;
private int count = 0;

public float moveSpeed;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        setCountText();
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * moveSpeed);

    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.tag);    
        if (other.gameObject.CompareTag("Pick Up"))
        {
            count += 1;
            other.gameObject.SetActive(false);
            setCountText();
        }
    }

    void setCountText()
    {
        countText.text = "Count: " + count.ToString();
    }

}
